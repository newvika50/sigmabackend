package sigma.sigmabackend.model.enums;

public enum UserStatus {
    CREATED, IN_PROGRESS, FAILED, SUCCESS
}
