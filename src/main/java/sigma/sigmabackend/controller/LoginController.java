package sigma.sigmabackend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import sigma.sigmabackend.model.User;
import sigma.sigmabackend.model.minor.LoginRequestBody;
import sigma.sigmabackend.repository.UserRepository;

import java.util.Optional;

@RestController
@RequestMapping("/auth")
@CrossOrigin
public class LoginController {
    @Autowired
    UserRepository userRepository;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @PostMapping("/login")
    public ResponseEntity<User> loginUser(@RequestBody LoginRequestBody loginRequestBody) {
        Optional<User> user = Optional.ofNullable(userRepository.findByEmail(loginRequestBody.login)/*.orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "No such User"))*/);
        if (bCryptPasswordEncoder.matches(loginRequestBody.password, user.get().getPassword())) {
            user.get().setPassword(loginRequestBody.password);
            return new ResponseEntity<>(user.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

}