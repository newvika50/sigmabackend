package sigma.sigmabackend.model.minor;

public class JudgeSubmission {
    public String source_code;
    public String language_id;
    public String stdin;
    public String expected_output;

    public JudgeSubmission(String source_code, String language_id, String stdin, String expected_output) {
        this.source_code = source_code;
        this.language_id = language_id;
        this.stdin = stdin;
        this.expected_output = expected_output;
    }

    @Override
    public String toString() {
        return "JudgeSubmission{" +
                "source_code='" + source_code + '\'' +
                ", language_id='" + language_id + '\'' +
                ", stdin='" + stdin + '\'' +
                ", expected_output='" + expected_output + '\'' +
                '}';
    }
}
