package sigma.sigmabackend.model.converters;

import sigma.sigmabackend.model.enums.TaskStatus;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class TaskStatusConverter implements AttributeConverter<TaskStatus, String> {
    @Override
    public String convertToDatabaseColumn(TaskStatus taskStatus) {
        switch (taskStatus) {
            case FAILED:
                return "failed";
            case ASSIGNED:
                return "assigned";
            case CANCELED:
                return "canceled";
            case COMPLETED:
                return "completed";
            default:
                throw new IllegalArgumentException("Unknown task status to convert to string:" + taskStatus);
        }
    }

    @Override
    public TaskStatus convertToEntityAttribute(String s) {
        switch (s) {
            case "failed":
                return TaskStatus.FAILED;
            case "assigned":
                return TaskStatus.ASSIGNED;
            case "canceled":
                return TaskStatus.CANCELED;
            case "completed":
                return TaskStatus.COMPLETED;
            default:
                throw new IllegalArgumentException("Unknown string to convert to task status:" + s);
        }
    }
}
