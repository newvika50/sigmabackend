package sigma.sigmabackend.model.minor;

public class CreateSubmissionResponse {
    public String token;

    public CreateSubmissionResponse(String token) {
        this.token = token;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        final CreateSubmissionResponse otherCreateSubmissionResponse = (CreateSubmissionResponse) obj;

        return otherCreateSubmissionResponse.token.equals(this.token);
    }
}
