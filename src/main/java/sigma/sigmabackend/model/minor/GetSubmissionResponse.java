package sigma.sigmabackend.model.minor;

public class GetSubmissionResponse {
    public String stdout;
    public String stderr;
    public String compile_output;
    public String message;
    public Integer exit_code;
    public Integer exit_signal;
    public String token;
    public Float time;
    public Float wall_time;
    public Float memory;
    public SubmissionStatus status;

    public GetSubmissionResponse() {

    }
}
