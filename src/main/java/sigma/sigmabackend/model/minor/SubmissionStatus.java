package sigma.sigmabackend.model.minor;

public class SubmissionStatus {
    public String description;
    public Integer id;

    public SubmissionStatus(String description, Integer id) {
        this.description = description;
        this.id = id;
    }
}
