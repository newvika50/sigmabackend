package sigma.sigmabackend.controller;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import sigma.sigmabackend.model.User;
import sigma.sigmabackend.model.minor.LoginRequestBody;
import sigma.sigmabackend.notification.EmailServiceImpl;
import sigma.sigmabackend.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    EmailServiceImpl emailService;


    @PostMapping
    public ResponseEntity<User> createUser(@RequestBody final User user) {
        int length = 10 + new Random().nextInt(15 - 5 + 1);
        String strongestPasswordEver = RandomStringUtils.random(length, true, true);
        user.setPassword(strongestPasswordEver);

        emailService.sendSimpleMessage(strongestPasswordEver, user.getEmail());
        User localUser = new User(user.getEmail(), user.getUsername(), bCryptPasswordEncoder.encode(user.getPassword()), user.getFirstName(), user.getLastName(),
                user.getPatronymic(), user.getPhone(), user.getInfo(), user.getRole());
        userRepository.save(localUser);
        localUser.setPassword(strongestPasswordEver);
        return new ResponseEntity<>(localUser, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<User> updateUser(@PathVariable("id") Long id, @RequestBody final User user) {
        Optional<User> oldUser = Optional.ofNullable(userRepository.findById(id).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "No such User")));
        if (oldUser.isPresent()) {
            User newUser = oldUser.get();
            newUser.setEmail(user.getEmail());
            newUser.setUsername(user.getUsername());
            newUser.setFirstName(user.getFirstName());
            newUser.setLastName(user.getLastName());
            newUser.setPatronymic(user.getPatronymic());
            newUser.setPhone(newUser.getPhone());
            newUser.setInfo(user.getInfo());
            newUser.setRole(newUser.getRole());

            return new ResponseEntity<>(userRepository.save(newUser), HttpStatus.OK);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No such User");
        }
    }

    @GetMapping
    public ResponseEntity<List<User>> getAllUsers() {
        List<User> userList = new ArrayList<User>();
        userRepository.findAll().forEach(userList::add);
        return new ResponseEntity<>(userList, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> geUserById(@PathVariable("id") Long id) {
        Optional<User> user = Optional.ofNullable(userRepository.findById(id).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "No such User")));
        return new ResponseEntity<>(user.get(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<User> deleteUserById(@PathVariable("id") Long id) {
        Optional<User> user = Optional.ofNullable(userRepository.findById(id).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "No such User")));
        userRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping("/login")
    public ResponseEntity<User> loginUser(@RequestBody LoginRequestBody loginRequestBody) {
        Optional<User> user = Optional.ofNullable(userRepository.findByEmail(loginRequestBody.login)/*.orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "No such User"))*/);
        if (bCryptPasswordEncoder.matches(loginRequestBody.password, user.get().getPassword())) {
            user.get().setPassword(loginRequestBody.password);
            return new ResponseEntity<>(user.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
