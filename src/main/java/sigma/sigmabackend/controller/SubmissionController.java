package sigma.sigmabackend.controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.*;
import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sigma.sigmabackend.model.Task;
import sigma.sigmabackend.model.UserTask;
import sigma.sigmabackend.model.minor.*;
import sigma.sigmabackend.repository.TaskRepository;
import sigma.sigmabackend.repository.UserTaskRepository;

import java.io.IOException;
import java.lang.reflect.Type;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/submissions")
@CrossOrigin
public class SubmissionController {
    private final OkHttpClient client = new OkHttpClient();

    Logger logger = LoggerFactory.logger(SubmissionController.class);

    @Autowired
    TaskRepository taskRepository;

    @Autowired
    UserTaskRepository userTaskRepository;

    @PostMapping("/test")
    public GetSubmissionResponse postSubmissionWithUserInput(@org.springframework.web.bind.annotation.RequestBody final UserInputRequest userInputRequest) {

        String code = userInputRequest.code;
        String input = userInputRequest.stdin;
        String language = userInputRequest.lang_id;

        code = code.replace("\n", "\\n");
        code = code.replace("\"", "\\\"");

        String json = String.format("{\"source_code\":\"%s\",\"language_id\":\"%s\",\"stdin\":\"%s\"}", code, language, input);

        com.squareup.okhttp.RequestBody requestBody = com.squareup.okhttp.RequestBody.create(MediaType.parse("application/json"), json);

        logger.info("Created body = " + json);
        Request request = new Request.Builder()
                .url("https://judge0-ce.p.rapidapi.com/submissions/?base64_encoded=false")
                .post(requestBody)
                .addHeader("X-RapidAPI-Host", "judge0-ce.p.rapidapi.com")
                .addHeader("X-RapidAPI-Key", "26f7fd743fmshbbda5007da6bafep166fefjsn8f6c7b5981c8")
                .addHeader("content-type", "application/json")
                .addHeader("Content-Type", "application/json")
                .build();

        Response response;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            throw new RuntimeException(e);
            //later return response that everything is bad
        }

        String responseBodyString;
        try (ResponseBody responseBody = response.body()) {
            responseBodyString = responseBody.string();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        logger.debug("Response in postSubmissionWithUserInput:" + responseBodyString);

        Gson gson = new Gson();
        Type responseType = new TypeToken<CreateSubmissionResponse>() {
        }.getType();
        CreateSubmissionResponse subResponse = gson.fromJson(responseBodyString, responseType);

        Request getRequest = new Request.Builder()
                .url("https://ce.judge0.com/submissions/" + subResponse.token)
                .get()
                .addHeader("X-RapidAPI-Host", "judge0-ce.p.rapidapi.com")
                .addHeader("X-RapidAPI-Key", "26f7fd743fmshbbda5007da6bafep166fefjsn8f6c7b5981c8")
                .build();

        //Add failsafe
        while (true) {
            try {
                Response getResponse = client.newCall(getRequest).execute();

                try (ResponseBody getResponseBody = getResponse.body()) {
                    String getResponseBodyString = getResponseBody.string();
                    logger.debug(getResponseBodyString);

                    gson = new Gson();
                    responseType = new TypeToken<GetSubmissionResponse>() {
                    }.getType();
                    GetSubmissionResponse getSubResponse = gson.fromJson(getResponseBodyString, responseType);

                    if (getSubResponse.status.id < 3) {
                        try {
                            TimeUnit.SECONDS.sleep(5);
                            continue;
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                    }
                    return getSubResponse;
                } catch (Exception e) {
                    logger.error(e.toString());
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @PostMapping("/finalize")
    public ResponseEntity<String> postFinalSubmission(@org.springframework.web.bind.annotation.RequestBody FinalizeRequest finalizeRequest) {

        //Get these from body request
        String jsonCode = finalizeRequest.code;
        String languageId = finalizeRequest.lang_id;


        Optional<UserTask> userTask = userTaskRepository.findById(Long.valueOf(finalizeRequest.userTask_id));

        if (userTask.isEmpty()) {
            return new ResponseEntity<>("error: not usertask with such id", HttpStatus.BAD_REQUEST);
        }

        Long taskId = userTask.get().getTaskId();

        Optional<Task> task = taskRepository.findById(taskId);

        if (task.isEmpty()) {
            return new ResponseEntity<>("error: no task with such id", HttpStatus.BAD_REQUEST);
        }

        String testsForJson = task.get().getTests().substring(1, task.get().getTests().length() - 1);

        Gson gson = new Gson();
        Type TestsJsonType = new TypeToken<TestsJson>() {
        }.getType();
        TestsJson tests = gson.fromJson(testsForJson, TestsJsonType);

        List<JudgeSubmission> submissions = new ArrayList<>();

        for (int i = 0; i < Math.min(tests.stdin.size(), tests.stdout.size()); i++) {
            submissions.add( new JudgeSubmission(jsonCode, languageId, tests.stdin.get(i) + "\n", tests.stdout.get(i)));
        }

        Type submissionsListType = new TypeToken<List<JudgeSubmission>>() {
        }.getType();
        String submissionsStr = gson.toJson(submissions, submissionsListType);

        logger.debug("submissionsStr:" + submissionsStr);

        String requestJson = String.format("{\"submissions\":%s}", submissionsStr);

        com.squareup.okhttp.RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), requestJson);
        Request request = new Request.Builder()
                .url("https://judge0-ce.p.rapidapi.com/submissions/batch?base64_encoded=false")
                .post(requestBody)
                .addHeader("X-RapidAPI-Host", "judge0-ce.p.rapidapi.com")
                .addHeader("X-RapidAPI-Key", "26f7fd743fmshbbda5007da6bafep166fefjsn8f6c7b5981c8")
                .addHeader("content-type", "application/json")
                .addHeader("Content-Type", "application/json")
                .build();

        Response response;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        String responseBodyString;
        try (ResponseBody responseBody = response.body()) {
            responseBodyString = responseBody.string();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        logger.info("Response in postFinalSubmission:" + responseBodyString);

        submissionsListType = new TypeToken<List<CreateSubmissionResponse>>() {
        }.getType();
        List<CreateSubmissionResponse> createSubmissionResponses = gson.fromJson(responseBodyString, submissionsListType);

        List<GetSubmissionResponse> submissionResults = new ArrayList<>();

        boolean areAllSubmissionsDone = false;

        while (!areAllSubmissionsDone) {
            List<String> doneSubmissions = new ArrayList<>();
            for (CreateSubmissionResponse submissionResponse : createSubmissionResponses) {
                if (doneSubmissions.contains(submissionResponse.token)) {
                    continue;
                }
                Request getRequest = new Request.Builder()
                        .url("https://ce.judge0.com/submissions/" + submissionResponse.token)
                        .get()
                        .addHeader("X-RapidAPI-Host", "judge0-ce.p.rapidapi.com")
                        .addHeader("X-RapidAPI-Key", "26f7fd743fmshbbda5007da6bafep166fefjsn8f6c7b5981c8")
                        .build();
                try {
                    Response getResponse = client.newCall(getRequest).execute();

                    try (ResponseBody getResponseBody = getResponse.body()) {
                        String getResponseBodyString = getResponseBody.string();
                        logger.debug(getResponseBodyString);

                        gson = new Gson();
                        Type responseType = new TypeToken<GetSubmissionResponse>() {
                        }.getType();
                        GetSubmissionResponse getSubResponse = gson.fromJson(getResponseBodyString, responseType);

                        if (getSubResponse.status.id >= 3) {
                            boolean isAdded = false;
                            for (GetSubmissionResponse subres : submissionResults) {
                                if (subres.token.equals(getSubResponse.token)) {
                                    isAdded = true;
                                    break;
                                }
                            }
                            if (!isAdded) {
                                submissionResults.add(getSubResponse);
                            }
                            doneSubmissions.add(getSubResponse.token);
                        }
                    } catch (Exception e) {
                        logger.error(e.toString());
                    }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
            if (createSubmissionResponses.size() == doneSubmissions.size()) {
                areAllSubmissionsDone = true;
            } else {
                try {
                    TimeUnit.SECONDS.sleep(5);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        StringBuilder taskResultBuilder = new StringBuilder();

        boolean areAllTestsGreen = true;

        for (GetSubmissionResponse submissionResult : submissionResults) {
            int testIndex = createSubmissionResponses.indexOf(new CreateSubmissionResponse(submissionResult.token));
            String input = tests.stdin.get(testIndex);
            switch (submissionResult.status.id) {
                case 3 -> {
                    taskResultBuilder.append(String.format("Test passed: stdin:%s, stdout:%s, time:%s, memory:%s\n", input, submissionResult.stdout, submissionResult.time, submissionResult.memory));
                }
                case 4 -> {
                    areAllTestsGreen = false;
                    taskResultBuilder.append(String.format("Test failed: stdin:%s, stdout:%s\n", input, submissionResult.stdout));
                }
                case 5 -> {
                    areAllTestsGreen = false;
                    taskResultBuilder.append(String.format("Test exceeded time limit: stdin:%s\n", input));
                }
                case 6 -> {
                    areAllTestsGreen = false;
                    taskResultBuilder.append(String.format("Test failed with compilation error: stdin:%s, compile_output:%s, message:%s\n", input, submissionResult.compile_output, submissionResult.message));
//                    taskResultBuilder.append(String.format("Test failed: stdin:%s\n", input));
                }
                default -> {
                    areAllTestsGreen = false;
                    taskResultBuilder.append(String.format("Test failed: stdin:%s, stderr: %s\n", input, submissionResult.stderr));
                }
            }
        }

        taskResultBuilder.insert(0, (areAllTestsGreen) ? "Testing passed\n" : "Testing failed\n");

        String resultString = taskResultBuilder.toString();

        UserTask newUserTask = userTask.get();
        newUserTask.setResult(resultString);
        newUserTask.setCode(finalizeRequest.code);
        if (areAllTestsGreen) {
            newUserTask.setStatus("Completed");
        } else {
            newUserTask.setStatus("Failed");
        }

        if (newUserTask.getSubmitDate() == null) {
            newUserTask.setSubmitDate(Instant.now());
        }
        userTaskRepository.save(newUserTask);

        //return that everything is okay
        return new ResponseEntity<>("success: uploaded results", HttpStatus.OK);
    }
}
