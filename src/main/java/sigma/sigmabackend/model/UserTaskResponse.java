package sigma.sigmabackend.model;

import java.time.Instant;

public class UserTaskResponse {

    private Long id;
    private Long taskId;
    private String taskName;
    private Long userId;
    private String userName;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String status;
    private Instant assignDate;
    private String result;
    private String code;
    private Instant startDate;
    private Instant submitDate;
    private String comment;

    public UserTaskResponse(Long id, Long taskId, String taskName, Long userId, String userName, String firstName, String lastName, String patronymic, String status, Instant assignDate, String result, String code, Instant startDate, Instant submitDate, String comment) {
        this.id = id;
        this.taskId = taskId;
        this.taskName = taskName;
        this.userId = userId;
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.status = status;
        this.assignDate = assignDate;
        this.result = result;
        this.code = code;
        this.startDate = startDate;
        this.submitDate = submitDate;
        this.comment = comment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Instant getAssignDate() {
        return assignDate;
    }

    public void setAssignDate(Instant assignDate) {
        this.assignDate = assignDate;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(Instant submitDate) {
        this.submitDate = submitDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
