package sigma.sigmabackend.model.enums;

public enum TaskStatus {
    ASSIGNED, CANCELED, FAILED, COMPLETED
}
