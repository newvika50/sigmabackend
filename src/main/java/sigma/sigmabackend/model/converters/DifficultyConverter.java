package sigma.sigmabackend.model.converters;

import sigma.sigmabackend.model.enums.Level;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class DifficultyConverter implements AttributeConverter<Level, String> {
    @Override
    public String convertToDatabaseColumn(Level level) {
        switch (level){

            case JUNIOR_BEGINNER:
                return "junior beginner";
            case JUNIOR_INTERMEDIATE:
                return "junior intermediate";
            case JUNIOR_ADVANCED:
                return "junior advanced";
            case MIDDLE_BEGINNER:
                return "middle beginner";
            case MIDDLE_INTERMEDIATE:
                return "middle intermediate";
            case MIDDLE_ADVANCED:
                return "middle advanced";
            case SENIOR_BEGINNER:
                return "senior beginner";
            case SENIOR_INTERMEDIATE:
                return "senior intermediate";
            case SENIOR_ADVANCED:
                return "senior advanced";
            default:
                throw new IllegalArgumentException("Unknown difficulty to convert to string:" + level);
        }
    }

    @Override
    public Level convertToEntityAttribute(String s) {
        switch (s) {
            case "junior beginner":
                return Level.JUNIOR_BEGINNER;
            case "junior intermediate":
                return Level.JUNIOR_INTERMEDIATE;
            case "junior advanced":
                return Level.JUNIOR_ADVANCED;
            case "middle beginner":
                return Level.MIDDLE_BEGINNER;
            case "middle intermediate":
                return Level.MIDDLE_INTERMEDIATE;
            case "middle advanced":
                return Level.MIDDLE_ADVANCED;
            case "senior beginner":
                return Level.SENIOR_BEGINNER;
            case "senior intermediate":
                return Level.SENIOR_INTERMEDIATE;
            case "senior advanced":
                return Level.SENIOR_ADVANCED;
            default:
                throw new IllegalArgumentException("Unknown string to convert to difficulty:" + s);
        }
    }
}
