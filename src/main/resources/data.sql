INSERT INTO roles (role) VALUES
('superuser'),
('candidate');

INSERT INTO user_basic_auth (login, password, email, role_id) VALUES
('admin', '$2a$12$.1zE7l.WdGRd8CqDiFkILuxyP.xHHGVsthhEfaRZV/gZwWsycuV7S', 'sigmaadmin@gmail.com', 1),
('testuser', '$2a$12$Ciow2WbWPWCv1ICIiga7ZOmfzDY4Qaey8.GzWwceTnm2/.0zkvC7W', 'testuser@mail.ru', 2);




INSERT INTO public.user_profile (id, email, username, password, first_name, last_name, patronymic, phone, info, role) VALUES (26, 'oleg@mail.com', 'ivan1', 'indkg4fg', 'Олег', 'Платонова', 'Антонович', '+79318773648', 'Студент', 'candidate');
INSERT INTO public.user_profile (id, email, username, password, first_name, last_name, patronymic, phone, info, role) VALUES (27, 'anton@mail.com', 'anton34', 'i2SGREg', 'Антон', 'Миронова', 'Юрьевич', '+79114773648', 'Студент', 'candidate');
INSERT INTO public.user_profile (id, email, username, password, first_name, last_name, patronymic, phone, info, role) VALUES (28, 'olga@mail.com', 'olga3', '234SDG', 'Ольга', 'Алексеева', 'Алексеевна', '+79118773548', 'Студент', 'candidate');
INSERT INTO public.user_profile (id, email, username, password, first_name, last_name, patronymic, phone, info, role) VALUES (29, 'alena@mail.com', 'alena', 'sfsgDG', 'Алена', 'Гончарова', 'Андреевна', '+75118773648', 'Студент', 'candidate');
INSERT INTO public.user_profile (id, email, username, password, first_name, last_name, patronymic, phone, info, role) VALUES (30, 'ivan@mail.com', 'ivanb', 'gaegRG', 'Иван', 'Бобров', 'Олегович', '+79116773648', 'Студент', 'candidate');
INSERT INTO public.user_profile (id, email, username, password, first_name, last_name, patronymic, phone, info, role) VALUES (31, 'andr@mail.com', 'andrei7', 'FKth67', 'Андрей', 'Мельников', 'Борисович', '+79118773648', '1 год опыта', 'candidate');
INSERT INTO public.user_profile (id, email, username, password, first_name, last_name, patronymic, phone, info, role) VALUES (32, 'vala@mail.com', 'valak', 'dh456fG', 'Валентина', 'Ковалева', 'Дмитриевна', '+79118778648', '1 год опыта', 'candidate');
INSERT INTO public.user_profile (id, email, username, password, first_name, last_name, patronymic, phone, info, role) VALUES (33, 'kkk@mail.com', 'kostia24', 'DF356FGH', 'Константин', 'Кузнецов', 'Кириллович', '+79198773648', '1 год опыта', 'candidate');
INSERT INTO public.user_profile (id, email, username, password, first_name, last_name, patronymic, phone, info, role) VALUES (34, 'irrina@mail.com', 'ira5', 'rghrhrh', 'Ирина', 'Петрова', 'Федоровна', '+79118773640', '2 года опыта', 'candidate');
INSERT INTO public.user_profile (id, email, username, password, first_name, last_name, patronymic, phone, info, role) VALUES (35, 'yurii@mail.com', 'yurii3', '464747', 'Юрий', 'Ткачев', 'Богданович', '+70118773648', '2 года опыта', 'candidate');


INSERT INTO public.tasks (id, name, topic_type, difficulty, description, tests) VALUES (28, 'Задача на сложение', 'Java', 'junior', 'Необходимо найти сумму двух чисел', '"{"stdin": ["50 1", "2 3", "9 0"],"stdout": ["51", "5", "9"]}"');
INSERT INTO public.tasks (id, name, topic_type, difficulty, description, tests) VALUES (29, 'Задача на вычитание', 'Java', 'junior', 'Необходимо найти разницу двух чисел', '"{"stdin": ["50 1", "3 2", "9 0"],"stdout": ["49", "1", "9"]}"');
INSERT INTO public.tasks (id, name, topic_type, difficulty, description, tests) VALUES (30, 'Задача на умножение', 'Java', 'junior', 'Необходимо вывести результат умножения двух чисел', '"{"stdin": ["50 1", "2 3", "9 0"],"stdout": ["50", "6", "0"]}"');
INSERT INTO public.tasks (id, name, topic_type, difficulty, description, tests) VALUES (31, 'Задача на деление', 'Java', 'junior', 'Необходимо вывести результат деления двух чисел', '"{"stdin": ["50 1", "6 2", "9 1"],"stdout": ["50", "3", "9"]}"');
INSERT INTO public.tasks (id, name, topic_type, difficulty, description, tests) VALUES (32, 'Задача на поиск максимума', 'Java', 'middle', 'Необходимо найти максимум в массиве целочисленных элементов', '"{"stdin": ["50", "2", ""],"stdout": ["99", "80", "99"]}"');
INSERT INTO public.tasks (id, name, topic_type, difficulty, description, tests) VALUES (33, 'Задача на поиск минимума', 'Java', 'middle', 'Необходимо найти минимум в массиве целочисленных элементов', ' "{"stdin": ["9", "", ""],"stdout": ["0", "0", "0"]}"');
INSERT INTO public.tasks (id, name, topic_type, difficulty, description, tests) VALUES (34, 'Сложение js', 'JS', 'junior', 'Необходимо найти сумму двух чисел', '"{"stdin": ["50 1", "2 3", "9 0"],"stdout": ["51", "5", "9"]}"');
INSERT INTO public.tasks (id, name, topic_type, difficulty, description, tests) VALUES (35, 'Вычитание js', 'JS', 'junior', 'Необходимо найти разницу двух чисел', '"{"stdin": ["50 1", "3 2", "9 0"],"stdout": ["49", "1", "9"]}"');
INSERT INTO public.tasks (id, name, topic_type, difficulty, description, tests) VALUES (36, 'Умножение js', 'JS', 'junior', 'Необходимо вывести результат умножения двух чисел', '"{"stdin": ["50 1", "2 3", "9 0"],"stdout": ["50", "6", "0"]}"');
INSERT INTO public.tasks (id, name, topic_type, difficulty, description, tests) VALUES (37, 'Деление js', 'JS', 'junior', 'Необходимо вывести результат деления двух чисел', '"{"stdin": ["50 1", "6 2", "9 1"],"stdout": ["50", "3", "9"]}"');


INSERT INTO public.user_tasks (id, task_id, user_id, status, assign_date, result, code) VALUES (1, 28, 26, 'assigned', '2022-06-17 12:07:08.000000', null, null);
INSERT INTO public.user_tasks (id, task_id, user_id, status, assign_date, result, code) VALUES (2, 29, 26, 'assigned', '2022-06-17 12:07:10.000000', null, null);
INSERT INTO public.user_tasks (id, task_id, user_id, status, assign_date, result, code) VALUES (3, 30, 26, 'assigned', '2022-06-17 12:07:11.000000', null, null);
INSERT INTO public.user_tasks (id, task_id, user_id, status, assign_date, result, code) VALUES (4, 31, 26, 'assigned', '2022-06-17 12:07:12.000000', null, null);
INSERT INTO public.user_tasks (id, task_id, user_id, status, assign_date, result, code) VALUES (5, 32, 27, 'assigned', '2022-06-17 12:07:12.000000', null, null);
INSERT INTO public.user_tasks (id, task_id, user_id, status, assign_date, result, code) VALUES (6, 33, 28, 'assigned', '2022-06-17 12:07:13.000000', null, null);
INSERT INTO public.user_tasks (id, task_id, user_id, status, assign_date, result, code) VALUES (7, 34, 29, 'assigned', '2022-06-17 12:07:13.000000', null, null);
INSERT INTO public.user_tasks (id, task_id, user_id, status, assign_date, result, code) VALUES (8, 35, 30, 'assigned', '2022-06-17 12:07:14.000000', null, null);
INSERT INTO public.user_tasks (id, task_id, user_id, status, assign_date, result, code) VALUES (9, 36, 27, 'assigned', '2022-06-17 12:07:15.000000', null, null);
INSERT INTO public.user_tasks (id, task_id, user_id, status, assign_date, result, code) VALUES (10, 37, 27, 'assigned', '2022-06-17 12:07:15.000000', null, null);