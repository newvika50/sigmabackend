package sigma.sigmabackend.notification;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service("EmailService")
public class EmailServiceImpl {

    @Autowired
    private JavaMailSender emailSender;

    public void sendSimpleMessage(String password, String email) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("worke8617@gmail.com");
        message.setTo(email);
        message.setSubject("Пароль от личного кабинета Sigma");
        message.setText("Ваш пароль: " + password);
        emailSender.send(message);
    }
}
