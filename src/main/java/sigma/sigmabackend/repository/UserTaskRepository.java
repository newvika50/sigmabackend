package sigma.sigmabackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sigma.sigmabackend.model.UserTask;

import java.util.List;

@Repository
public interface UserTaskRepository extends JpaRepository<UserTask, Long> {
    List<UserTask> findAllByUserId(Long id);

    UserTask findByUserIdAndTaskId(Long taskId, Long userId);
}
