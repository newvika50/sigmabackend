package sigma.sigmabackend.model;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "user_tasks")
public class UserTask {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "task_id", nullable = false)
    private Long taskId;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "status", nullable = false)
    private String status;

    @Column(name = "assign_date", nullable = false)
    private Instant assignDate;

    @Column(name = "result", nullable = false)
    private String result;

    @Column(name = "code", nullable = false)
    private String code;

    @Column(name ="start_date", nullable = true)
    private Instant startDate;

    @Column(name = "submit_date", nullable = true)
    private Instant submitDate;

    @Column(name = "comment", nullable = true)
    private String comment;

    public UserTask(Long taskId, Long userId, String status, Instant assignDate, String result, String code, String comment) {
        this.taskId = taskId;
        this.userId = userId;
        this.status = status;
        this.assignDate = assignDate;
        this.result = result;
        this.code = code;
        this.comment = comment;
    }

    public UserTask() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Instant getAssignDate() {
        return assignDate;
    }

    public void setAssignDate(Instant assignDate) {
        this.assignDate = assignDate;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(Instant submitDate) {
        this.submitDate = submitDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
