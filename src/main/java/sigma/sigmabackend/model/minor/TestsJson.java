package sigma.sigmabackend.model.minor;

import java.util.List;

public class TestsJson {
    public List<String> stdin;

    public List<String> stdout;

    public TestsJson(List<String> stdin, List<String> stdout) {
        this.stdin = stdin;
        this.stdout = stdout;
    }
}
