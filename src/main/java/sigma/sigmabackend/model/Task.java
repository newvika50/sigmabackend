package sigma.sigmabackend.model;

import javax.persistence.*;

@Entity
@Table(name = "tasks")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "topic_type", nullable = false)
    private String topicType;

    @Column(name = "difficulty", nullable = false)
    private String difficulty;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "tests", nullable = false)
    private String tests;

    public Task(String name, String topicType, String difficulty, String description, String tests) {
        this.name = name;
        this.topicType = topicType;
        this.difficulty = difficulty;
        this.description = description;
        this.tests = tests;
    }

    public Task() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTopicType() {
        return topicType;
    }

    public void setTopicType(String topicType) {
        this.topicType = topicType;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTests() {
        return tests;
    }

    public void setTests(String tests) {
        this.tests = tests;
    }
}
