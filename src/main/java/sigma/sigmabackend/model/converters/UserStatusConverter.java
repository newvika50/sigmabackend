package sigma.sigmabackend.model.converters;

import sigma.sigmabackend.model.enums.UserStatus;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class UserStatusConverter implements AttributeConverter<UserStatus, String> {
    @Override
    public String convertToDatabaseColumn(UserStatus userStatus) {
        switch (userStatus) {
            case SUCCESS:
                return "success";
            case FAILED:
                return "failed";
            case CREATED:
                return "created";
            case IN_PROGRESS:
                return "in_progress";
            default:
                throw new IllegalArgumentException("Unknown user status to convert to string:" + userStatus);
        }
    }

    @Override
    public UserStatus convertToEntityAttribute(String s) {
        switch (s) {
            case "success":
                return UserStatus.SUCCESS;
            case "failed":
                return UserStatus.FAILED;
            case "created":
                return UserStatus.CREATED;
            case "in_progress":
                return UserStatus.IN_PROGRESS;
            default:
                throw new IllegalArgumentException("Unknown string to convert to user status:" + s);
        }
    }
}
