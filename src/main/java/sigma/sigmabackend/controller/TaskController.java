package sigma.sigmabackend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import sigma.sigmabackend.model.Task;
import sigma.sigmabackend.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/task")
@CrossOrigin
public class TaskController {

    @Autowired
    TaskRepository taskRepository;

    @PostMapping
    public ResponseEntity<Task> createTask(@RequestBody final Task task) {
        Task localTask = new Task(task.getName(), task.getTopicType(), task.getDifficulty(), task.getDescription(),
                task.getTests());
        taskRepository.save(localTask);
        return new ResponseEntity<>(localTask, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Task> updateTask(@PathVariable("id") Long id, @RequestBody final Task task) {
        Optional<Task> oldTask = Optional.ofNullable(taskRepository.findById(id).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "No such Task")));
        if (oldTask.isPresent()) {
            Task newTask = oldTask.get();
            newTask.setName(task.getName());
            newTask.setTopicType(task.getTopicType());
            newTask.setDifficulty(task.getDifficulty());
            newTask.setDescription(task.getDescription());
            newTask.setTests(task.getTests());

            return new ResponseEntity<>(taskRepository.save(newTask), HttpStatus.OK);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No such Task");
        }
    }

    @GetMapping
    public ResponseEntity<List<Task>> getAllTasks() {
        List<Task> taskList = new ArrayList<Task>();
        taskRepository.findAll().forEach(taskList::add);
        return new ResponseEntity<>(taskList, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Task> getTaskById(@PathVariable("id") Long id) {
        Optional<Task> task = Optional.ofNullable(taskRepository.findById(id).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "No such Task")));
        return new ResponseEntity<>(task.get(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Task> deleteTaskById(@PathVariable("id") Long id) {
        Optional<Task> task = Optional.ofNullable(taskRepository.findById(id).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "No such Task")));
        taskRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
