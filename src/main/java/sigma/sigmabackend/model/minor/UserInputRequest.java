package sigma.sigmabackend.model.minor;

public class UserInputRequest {
    public String lang_id;

    public String code;

    public String stdin;

    public UserInputRequest(String lang_id, String code, String stdin) {
        this.lang_id = lang_id;
        this.code = code;
        this.stdin = stdin;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        final UserInputRequest otherUserInputRequest = (UserInputRequest) obj;

        return otherUserInputRequest.lang_id.equals(this.lang_id) && otherUserInputRequest.code.equals(this.code) && otherUserInputRequest.stdin.equals(this.stdin);
    }
}
