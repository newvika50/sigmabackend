package sigma.sigmabackend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import sigma.sigmabackend.model.UserTask;
import sigma.sigmabackend.model.UserTaskResponse;
import sigma.sigmabackend.repository.UserTaskRepository;

import java.sql.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/user_task")
@CrossOrigin
public class UserTaskController {

    @Autowired
    UserTaskRepository userTaskRepository;

    @Autowired
    public Environment environment;

    @PostMapping
    public ResponseEntity<UserTask> createUserTask(@RequestBody final UserTask userTask) {
        UserTask localUserTask = new UserTask(userTask.getTaskId(), userTask.getUserId(), "assigned",
                Instant.now(), userTask.getResult(), userTask.getCode(), userTask.getComment());
        userTaskRepository.save(localUserTask);
        return new ResponseEntity<>(localUserTask, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserTask> updateUserTask(@PathVariable("id") Long id, @RequestBody final UserTask userTask) {
        Optional<UserTask> oldUserTask = Optional.ofNullable(userTaskRepository.findById(id).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "No such User Task")));
        if (oldUserTask.isPresent()) {
            UserTask newUserTask = oldUserTask.get();
            newUserTask.setTaskId(userTask.getTaskId());
            newUserTask.setUserId(userTask.getUserId());
            newUserTask.setStatus(userTask.getStatus());
            newUserTask.setAssignDate(userTask.getAssignDate());
            newUserTask.setResult(userTask.getResult());
            newUserTask.setCode(userTask.getCode());

            return new ResponseEntity<>(userTaskRepository.save(newUserTask), HttpStatus.OK);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No such User Task");
        }
    }

    @GetMapping
    public ResponseEntity<List<UserTaskResponse>> getAllUserTask() {
        List<UserTaskResponse> userTaskResponseList = new ArrayList<UserTaskResponse>();
        List<UserTask> userTaskList = new ArrayList<UserTask>();
        userTaskRepository.findAll().forEach(userTaskList::add);

        //@TODO extract to separate method
        String databaseUrl = environment.getProperty("spring.datasource.url");
        String databaseUsername = environment.getProperty("spring.datasource.username");
        String databasePassword = environment.getProperty("spring.datasource.password");

        try {
            Connection connection = DriverManager.getConnection(databaseUrl, databaseUsername, databasePassword);

            if(connection != null) {
                for(UserTask userTask: userTaskList) {
                    String sqlQuery = String.format("SELECT user_profile.username, user_profile.first_name, user_profile.last_name, user_profile.patronymic, tasks.name " +
                            "FROM user_profile, user_tasks, tasks WHERE user_tasks.user_id=user_profile.id AND user_tasks.task_id=tasks.id AND user_tasks.id=%d", userTask.getId());
                    Statement statement = connection.createStatement();
                    ResultSet resultSet = statement.executeQuery(sqlQuery);
                    while(resultSet.next()) {
                        String userName = resultSet.getString("username");
                        String firstName = resultSet.getString("first_name");
                        String lastName = resultSet.getString("last_name");
                        String patronymic = resultSet.getString("patronymic");
                        String taskName = resultSet.getString("name");

                        UserTaskResponse userTaskResponse = new UserTaskResponse(
                                userTask.getId(),
                                userTask.getTaskId(),
                                taskName,
                                userTask.getUserId(),
                                userName,
                                firstName,
                                lastName,
                                patronymic,
                                userTask.getStatus(),
                                userTask.getAssignDate(),
                                userTask.getResult(),
                                userTask.getCode(),
                                userTask.getStartDate(),
                                userTask.getSubmitDate(),
                                userTask.getComment()
                        );

                        userTaskResponseList.add(userTaskResponse);
                    }

                    resultSet.close();
                }

                connection.close();
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }

        return new ResponseEntity<>(userTaskResponseList, HttpStatus.OK);
    }

    @GetMapping("/userTaskId:{id}")
    public ResponseEntity<UserTask> geUserTaskById(@PathVariable("id") Long id) {
        Optional<UserTask> userTask = userTaskRepository.findById(id);
        return new ResponseEntity<>(userTask.get(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<List<UserTaskResponse>> geUserTaskByUserId(@PathVariable("id") Long id) {
        List<UserTaskResponse> userTaskResponseList = new ArrayList<UserTaskResponse>();
        List<UserTask> userTask = userTaskRepository.findAllByUserId(id).stream()
                .filter(e -> e.getUserId().equals(id))
                .collect(Collectors.toList());

        //@TODO extract to separate method
        String databaseUrl = environment.getProperty("spring.datasource.url");
        String databaseUsername = environment.getProperty("spring.datasource.username");
        String databasePassword = environment.getProperty("spring.datasource.password");

        try {
            Connection connection = DriverManager.getConnection(databaseUrl, databaseUsername, databasePassword);

            if(connection != null) {
                for(UserTask localUserTask: userTask) {
                    if (localUserTask.getStartDate() == null) {
                        localUserTask.setStartDate(Instant.now());
                    }

                    String sqlQuery = String.format("SELECT user_profile.username, user_profile.first_name, user_profile.last_name, user_profile.patronymic, tasks.name " +
                            "FROM user_profile, user_tasks, tasks WHERE user_tasks.user_id=user_profile.id AND user_tasks.task_id=tasks.id AND user_tasks.id=%d", localUserTask.getId());
                    Statement statement = connection.createStatement();
                    ResultSet resultSet = statement.executeQuery(sqlQuery);
                    while(resultSet.next()) {
                        String userName = resultSet.getString("username");
                        String firstName = resultSet.getString("first_name");
                        String lastName = resultSet.getString("last_name");
                        String patronymic = resultSet.getString("patronymic");
                        String taskName = resultSet.getString("name");

                        UserTaskResponse userTaskResponse = new UserTaskResponse(
                                localUserTask.getId(),
                                localUserTask.getTaskId(),
                                taskName,
                                localUserTask.getUserId(),
                                userName,
                                firstName,
                                lastName,
                                patronymic,
                                localUserTask.getStatus(),
                                localUserTask.getAssignDate(),
                                localUserTask.getResult(),
                                localUserTask.getCode(),
                                localUserTask.getStartDate(),
                                localUserTask.getSubmitDate(),
                                localUserTask.getComment()
                        );

                        userTaskResponseList.add(userTaskResponse);
                    }

                    resultSet.close();
                }

                connection.close();
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }

        return new ResponseEntity<>(userTaskResponseList, HttpStatus.OK);
    }

    @GetMapping("/{userId}/{taskId}")
    public ResponseEntity<Long> getUserTaskIdByUserIdAndTaskId(@PathVariable("userId") Long userId, @PathVariable Long taskId) {
        UserTask userTask = userTaskRepository.findByUserIdAndTaskId(userId, taskId);
        if (userTask.getStartDate() == null) {
            userTask.setStartDate(Instant.now());
        }
        userTaskRepository.save(userTask);
        return new ResponseEntity<Long>(userTask.getId(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<UserTask> deleteUserTaskById(@PathVariable("id") Long id) {
        Optional<UserTask> userTask = Optional.ofNullable(userTaskRepository.findById(id).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "No such User Task")));
        userTaskRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
