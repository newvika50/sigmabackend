CREATE TYPE user_status AS ENUM ('created', 'in_progress', 'failed' , 'succsess');
CREATE TYPE task_status AS ENUM ('assigned','canceled', 'failed', 'completed');
CREATE TYPE level AS ENUM ('junior_beginner', 'junior_intermediate','junior_advanced','middle_beginner','middle_intermediate',
'middle_advanced', 'senior_beginner', 'senior_intermediate','senior_advanced');

CREATE TABLE roles (
    id SERIAL PRIMARY KEY,
    role TEXT NOT NULL UNIQUE
);

CREATE TABLE topics (
    id SERIAL PRIMARY KEY,
    topic TEXT NOT NULL UNIQUE
);

INSERT INTO topics (topic) VALUES 
('js'),
('java');

INSERT INTO roles (role) VALUES 
('superuser'),
('candidate');

/*CREATE TABLE user_basicAuth (
    id SERIAL PRIMARY KEY,
    login TEXT NOT NULL UNIQUE,
    password TEXT NOT NULL UNIQUE,
    email TEXT NOT NULL UNIQUE,
    role_id INTEGER NOT NULL REFERENCES roles (id)
);*/

/*CREATE TABLE user_profile (
    id SERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL REFERENCES user_basicAuth (id),
    first_name TEXT NOT NULL,
    last_name TEXT NOT NULL,
    patronymic TEXT NOT NULL,
    phone TEXT NOT NULL UNIQUE,
    status user_status NOT NULL,
    info TEXT
);*/

CREATE TABLE user_profile
(
    id         SERIAL PRIMARY KEY,
    email      TEXT NOT NULL UNIQUE,
    username   TEXT NOT NULL,
    password   TEXT NOT NULL,
    first_name TEXT,
    last_name  TEXT,
    patronymic TEXT,
    phone      TEXT NOT NULL UNIQUE,
    info       TEXT,
    role       TEXT NOT NULL
);

CREATE TABLE tasks
(
    id          SERIAL PRIMARY KEY,
    name        TEXT NOT NULL UNIQUE,
    topic_type  TEXT NOT NULL,
    difficulty  TEXT NOT NULL,
    description TEXT NOT NULL,
    tests       TEXT NOT NULL
);

CREATE TABLE user_tasks
(
    id          SERIAL PRIMARY KEY,
    task_id     INTEGER   NOT NULL REFERENCES tasks (id),
    user_id     INTEGER   NOT NULL REFERENCES user_profile (id),
    status      TEXT      NOT NULL,
    assign_date TIMESTAMP NOT NULL,
    result      TEXT,
    code        TEXT,
    start_date  TIMESTAMP,
    submit_date TIMESTAMP,
    comment     TEXT
);

INSERT INTO user_profile (email, username, password, first_name, last_name, patronymic, phone, info, role) VALUES ('superuser', '123', '$2a$12$4TnTU137GfBRMvT9kqaBau.SlRf6uwHjgQV2Bhk4mBxhPb0JESSsq', 'superuser', '1', '1', '1', '1', 'superuser');
INSERT INTO user_profile (email, username, password, first_name, last_name, patronymic, phone, info, role) VALUES ('user', '123', '$2a$12$4TnTU137GfBRMvT9kqaBau.SlRf6uwHjgQV2Bhk4mBxhPb0JESSsq', 'user', '1', '1', '2', '1', 'user');
